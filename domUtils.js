/**
 * Throughout, whitespace is defined as one of the characters
 *  "\t" TAB \u0009
 *  "\n" LF  \u000A
 *  "\r" CR  \u000D
 *  " "  SPC \u0020
 *
 * This does not use Javascript's "\s" because that includes non-breaking
 * spaces (and also some other characters).
 */


/**
 * Determine whether a node's text content is entirely whitespace.
 *
 * @param nod  A node implementing the |CharacterData| interface (i.e.,
 *             a |Text|, |Comment|, or |CDATASection| node
 * @return     True if all of the text content of |nod| is whitespace,
 *             otherwise false.
 */
function is_all_ws(nod) {
    // Use ECMA-262 Edition 3 String and RegExp features
    return !(/[^\t\n\r ]/.test(nod.textContent));
}


/**
 * Determine if a node should be ignored by the iterator functions.
 *
 * @param nod  An object implementing the DOM1 |Node| interface.
 * @return     true if the node is:
 *                1) A |Text| node that is all whitespace
 *                2) A |Comment| node
 *                  3) A |Processing Instruction| node
 *             and otherwise false.
 */

function is_ignorable(nod) {
    return (nod.nodeType == 7) || // A processing instruction mode
        (nod.nodeType == 8) || // A comment node
        ((nod.nodeType == 3) && is_all_ws(nod)); // a text node, all ws
}

/**
 * Determine if a node is terminal (has no children).
 *
 * @param nod  An object implementing the DOM1 |Node| interface.
 * @return     true if the node is:
 *                terminal
 *             otherwise false
 */

function is_terminal(nod) {
    return !nod.hasChildNodes();
}

/**
 * Determine if a node is terminal (has no children).
 *
 * @param nod  An object implementing the DOM1 |Node| interface.
 * @return     true if the node is:
 *                terminal
 *             otherwise false
 */

function is_variableNode(node) {
    return getNameWithoutNSPrefix(node.nodeName) === "variable";
}

function isNodeWithoutVarRefs(node) {
    return !node.innerHTML.includes("$");
}

/**
 * Version of |previousSibling| that skips nodes that are entirely
 * whitespace or comments.  (Normally |previousSibling| is a property
 * of all DOM nodes that gives the sibling node, the node that is
 * a child of the same parent, that occurs immediately before the
 * reference node.)
 *
 * @param sib  The reference node.
 * @return     Either:
 *               1) The closest previous sibling to |sib| that is not
 *                  ignorable according to |is_ignorable|, or
 *               2) null if no such node exists.
 */
function node_before(sib) {
    while ((sib = sib.previousSibling)) {
        if (!is_ignorable(sib)) return sib;
    }
    return null;
}

/**
 * Version of |nextSibling| that skips nodes that are entirely
 * whitespace or comments.
 *
 * @param sib  The reference node.
 * @return     Either:
 *               1) The closest next sibling to |sib| that is not
 *                  ignorable according to |is_ignorable|, or
 *               2) null if no such node exists.
 */
function node_after(sib) {
    while ((sib = sib.nextSibling)) {
        if (!is_ignorable(sib)) return sib;
    }
    return null;
}

/**
 * Version of |lastChild| that skips nodes that are entirely
 * whitespace or comments.  (Normally |lastChild| is a property
 * of all DOM nodes that gives the last of the nodes contained
 * directly in the reference node.)
 *
 * @param sib  The reference node.
 * @return     Either:
 *               1) The last child of |sib| that is not
 *                  ignorable according to |is_ignorable|, or
 *               2) null if no such node exists.
 */
function last_child(par) {
    var res = par.lastChild;
    while (res) {
        if (!is_ignorable(res)) return res;
        res = res.previousSibling;
    }
    return null;
}

/**
 * Version of |firstChild| that skips nodes that are entirely
 * whitespace and comments.
 *
 * @param sib  The reference node.
 * @return     Either:
 *               1) The first child of |sib| that is not
 *                  ignorable according to |is_ignorable|, or
 *               2) null if no such node exists.
 */
function first_child(par) {
    var res = par.firstChild;
    while (res) {
        if (!is_ignorable(res)) return res;
        res = res.nextSibling;
    }
    return null;
}

function getChildren(par) {
    var res = [];
    var current = par.firstChild;
    while (current) {
        if (!is_ignorable(current))
            res.push(current);
        current = current.nextSibling;
    }
    return res;
}

/**
 * Version of |data| that doesn't include whitespace at the beginning
 * and end and normalizes all whitespace to a single space.  (Normally
 * |data| is a property of text nodes that gives the text of the node.)
 *
 * @param txt  The text node whose data should be returned
 * @return     A string giving the contents of the text node with
 *             whitespace collapsed.
 */
function data_of(txt) {
    var data = txt.textContent;
    // Use ECMA-262 Edition 3 String and RegExp features
    data = data.replace(/[\t\n\r ]+/g, " ");
    if (data.charAt(0) == " ")
        data = data.substring(1, data.length);
    if (data.charAt(data.length - 1) == " ")
        data = data.substring(0, data.length - 1);
    return data;
}

function getTerminalElements(xmlDocElement) {
    let treeWalker = document.createTreeWalker(
        xmlDocElement,
        NodeFilter.SHOW_ALL,
        {
            acceptNode: function (node) {
                return !is_ignorable(node) && is_terminal(node) ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
            }
        }
    );
    let terminalNodes = [];
    let currentNode = treeWalker.nextNode();
    while (currentNode) {
        terminalNodes.push(currentNode);
        currentNode = treeWalker.nextNode();
    }
    return terminalNodes;
}

function getOracleMappingParams(xmlDocElement) {
    let treeWalker = document.createTreeWalker(
        xmlDocElement,
        NodeFilter.SHOW_ALL,
        {
            acceptNode: function (node) {
                return node.nodeType === Node.PROCESSING_INSTRUCTION_NODE && node.nodeName === "oracle-xsl-mapper" ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
            }
        }
    );
    let resultNodes = [];
    let currentNode = treeWalker.nextNode();
    while (currentNode) {
        resultNodes.push(currentNode);
        currentNode = treeWalker.nextNode();
    }
    if (resultNodes.length > 1) {
        alert('The XSL File contains more than one processing instruction elements "oracle-xsl-mapper".');
        return;
    }
    let parser = new DOMParser();
    let root = parser.parseFromString("<root>" + resultNodes[0].nodeValue + "</root>", "text/xml");
    if (root) {
        let result = {};
        result.sources = [];
        result.targets = [];
        let sources = root.getElementsByTagName("source");
        for (let i = 0; i < sources.length; i++) {
            let src = {};
            src.toString = function () {
                return src.location + "\n" + "Объект " + src.rootNode;
            };
            src.location = sources.item(i).getElementsByTagName("schema")[0].getAttribute("location").replace("oramds:/apps/", "");
            src.rootNode = sources.item(i).getElementsByTagName("rootElement")[0].getAttribute("name");
            result.sources.push(src);
        }
        let targets = root.getElementsByTagName("target");
        for (let i = 0; i < targets.length; i++) {
            let tgt = {};
            tgt.toString = function () {
                return tgt.location + "\n" + "Объект " + tgt.rootNode;
            };
            tgt.location = targets.item(i).getElementsByTagName("schema")[0].getAttribute("location").replace("oramds:/apps/", "");
            tgt.rootNode = targets.item(i).getElementsByTagName("rootElement")[0].getAttribute("name");
            result.targets.push(tgt);
        }
        result.stringArr = toString();

        function toString() {
            let resultStr = [];

            function arrayToString(array) {
                let result = array.map(e => e.toString());
                return result.join("\n");
            }

            resultStr.push(arrayToString(result.sources));
            resultStr.push(arrayToString(result.targets));
            return resultStr;
        }

        return result;
    }
}

function getNameWithoutNSPrefix(nodeName) {
    if (nodeName) {
        return nodeName.replace(/[a-z0-9]+:/, "");
    }
}

function removeQuotes(str) {
    if (str) {
        return str.replace(/['"]+/g, '');
    }
}


function getPathUntilParent(node, parentNode) {
    parentNode = parentNode ? $(parentNode) : $(xmlDocElement).find('xsl\\:template[match="/"]')[0];
    return $(node).parentsUntil(parentNode).toArray()
        .reverse()
        .map(e => {
            let nodeName = getNameWithoutNSPrefix(e.nodeName);
            return nodeName === "attribute" ? `@${e.getAttributeNode("name").value}` : nodeName;
        })
        .join("/")
        .replace(/(\/text$)/, "")
        .replace(/(\/if|\/when)/g, "");
}

function getTerminalNodesValue(node) {
    if (is_terminal(node) && !is_ignorable(node)) {
        if (getNameWithoutNSPrefix(node.parentNode.nodeName) === "text") { //Const
            return `Const "${node.nodeValue}"`;
        } else if (node.nodeType === Node.ELEMENT_NODE && getNameWithoutNSPrefix(node.nodeName) === "value-of") {
            let valueWithoutPrefixes = node.getAttributeNode("select").value.replace(/([a-z0-9]+:|^\/)/g, "");
            if (valueWithoutPrefixes[0] === valueWithoutPrefixes[valueWithoutPrefixes.length - 1] && valueWithoutPrefixes[0] === ("'" || '"')) {
                return `Const ${valueWithoutPrefixes.replace(/(')/g, '"')}`;
            }
            return replaceByDictionary(valueWithoutPrefixes);
        }
    }
}

function forEachHandler(xmlDocElement) {
    let terminalNodes = getTerminalElements(xmlDocElement);
    let ignoredTerminalNodes = [];
    let foreachObj = {};
    for (let terminalNode of terminalNodes) {
        if (!ignoredTerminalNodes.includes(terminalNode)) {
            let parentNode = terminalNode.parentNode;
            while (parentNode) {
                if (getNameWithoutNSPrefix(parentNode.nodeName) === "for-each") {
                    getTerminalElements(parentNode).forEach(terminalElement=>{
                        ignoredTerminalNodes.push(terminalElement);
                        terminalElement.foreachElement = parentNode;
                    })
                }
                parentNode = parentNode.parentNode;
            }
        }
    }
}

function getSourceValue(srcPath) {
    let valueWithoutPrefixes = srcPath.replace(/([a-z0-9]+:|^\/)/g, "");
    return replaceByDictionary(valueWithoutPrefixes);
}

function getMappingType(node) {
    let srcPath = getTerminalNodesValue(node);
    if (srcPath) {
        if (srcPath.includes("tsc") || srcPath.includes("dvm")) {
            return "не прямое";
        } else return "прямое";
    }
}

function chooseNodeToText(node) {
    let resultStr = "";
    if (getNameWithoutNSPrefix(node.nodeName) === "choose") {
        for (let conditionNode of getChildren(node)) {
            let terminalNodes = getTerminalElements(conditionNode);
            for (let terminalNode of terminalNodes) {
                let valueStr = replaceByDictionary(getTerminalNodesValue(terminalNode)).str;
                if (getNameWithoutNSPrefix(conditionNode.nodeName) === "when") {
                    let conditionStr = getSourceValue(conditionNode.getAttributeNode("test").value);
                    resultStr += `Если ${conditionStr}, то ${getPathUntilParent(terminalNode, conditionNode)}=${valueStr}\n`;
                } else {
                    resultStr += `иначе ${getPathUntilParent(terminalNode, conditionNode)}=${valueStr}\n`
                }
            }
        }
        return resultStr;
    }
}

var dictionary = {
    func: {
        "string-length": "длина строки",
        "current-dateTime": "текущая дата/время",
        "getSEBLDateTime": "дата и время в формате SEBL",
        "getSEBLDate": "дата в формате SEBL",
        "lookupValue": function (paramsArr) {
            if (paramsArr.length >= 4) {
                let templateStr = `Преобразовать по справочнику ${paramsArr[0]}, значение (${paramsArr[2]}), ${paramsArr[1]}=>${paramsArr[3]}`;
                let qualifierStr = "";
                let defaultValue;
                if (paramsArr.length % 2) {
                    defaultValue = paramsArr[4];
                    templateStr = `${templateStr}, если значение не нашлось, то ${defaultValue}`;
                }
                for (let i = !(paramsArr.length % 2) ? 4 : 5; i < paramsArr.length; i += 2)
                    qualifierStr += `${paramsArr[i]}=${paramsArr[i + 1]} `;
                return `${templateStr}, где ${qualifierStr}.`;
            }
        },
        getDereferencedName: function (propertyName, params = []) {
            if (typeof dictionary.func[propertyName] === "function") {
                return dictionary.func[propertyName](params);
            } else return `${dictionary.func[propertyName]} ${params[0]}`;
        },
    },
    sign: {
        "!=": " не равно ",
        ">": " больше ",
        "<": " меньше ",
        ">=": " больше либо равно ",
        "&gt;": " больше либо равно ",
        "<=": " меньше либо равно ",
        "&lt;": " меньше либо равно ",
        " or ": "или",
        " and ": "и",
        "=": " равно ",
        getDereferencedName: function (propertyName) {
            return dictionary.sign[propertyName];
        }
    },
    getDereferencedName: function (propertyName) {
        return dictionary[propertyName];
    }

};

function replaceByDictionary(string) {
    let resultObject={};
    let replaceValues = [];
    let indexLastReplacedValue = 0;
    Object.keys(dictionary.sign).filter(key => key !== "getDereferencedName").forEach(key => {
        let regexp = new RegExp(key, "g");
        string = string.replace(regexp, dictionary.sign.getDereferencedName(key));
    });
    Object.keys(dictionary.func).filter(key => key !== "getDereferencedName").forEach(key => {
        let regexp = `(tsc:|dvm:)?${key}[\\s]*\\(([^\\(\\)]*)\\)`;
        let matchAll = Array.from(string.matchAll(regexp));
        let innerParam = true;
        while (matchAll.length) {
            matchAll.forEach(match => {
                let functionParams = match[2] !== "" ?
                    match[2].split(',').map(item => {
                        return item ? removeQuotes(item.trim()) : [""];
                    }) : [""];
                if (innerParam){
                    innerParam = false;
                    resultObject.funcParam = innerParam;
                }
                let dereferencedFunctionName = dictionary.func.getDereferencedName(key, functionParams);
                replaceValues.push(dereferencedFunctionName);
                string = string.replace(match[0], `replaceValue${indexLastReplacedValue}`);
                indexLastReplacedValue++;
            });
            matchAll = Array.from(string.matchAll(regexp));
        }
    });
    for (let i = replaceValues.length - 1; i >= 0; i--)
        string = string.replace(`replaceValue${i}`, replaceValues[i]);
    resultObject.str = string.replace(/\s+/g, ' ').trim();
    return resultObject;
}

function variablesDereference(xmlDocElement, variables, xmlDocElementStr) {
    let allVarsWithoutRefs = true;
    if (variables === undefined) {
        variables = [];
    }
    let treeWalker = document.createTreeWalker(
        xmlDocElement,
        NodeFilter.SHOW_ALL,
        {
            acceptNode: function (node) {
                if (!is_ignorable(node)) {
                    if (is_variableNode(node)) {
                        if (isNodeWithoutVarRefs(node)) {
                            return NodeFilter.FILTER_ACCEPT;
                        } else {
                            allVarsWithoutRefs = false;
                            return NodeFilter.FILTER_SKIP;
                        }
                    } else return NodeFilter.FILTER_SKIP;
                } else return NodeFilter.FILTER_SKIP;
            }
        }
    );
    let currentNode = treeWalker.nextNode();
    xmlDocElementStr = xmlDocElementStr === undefined ? new XMLSerializer().serializeToString(xmlDocElement.documentElement) : xmlDocElementStr;
    while (currentNode) {
        variables.push(currentNode);
        let value;
        let selectAttr = currentNode.getAttributeNode("select");
        if (selectAttr) {
            value = selectAttr.value.replace(/([a-z0-9]+:|^\/)/g, "");
        } else if (first_child(currentNode) && getNameWithoutNSPrefix(first_child(currentNode).nodeName) === "choose") {
            value = chooseNodeToText(first_child(currentNode));
        }
        let regexp = new RegExp(`\\$${currentNode.getAttributeNode("name").value}`, "g");
        xmlDocElementStr = xmlDocElementStr.replace(regexp, value);
        currentNode = treeWalker.nextNode();
    }
    if (allVarsWithoutRefs)
        return xmlDocElementStr;
    else
        return variablesDereference(xmlDocElement, variables, xmlDocElementStr);
}


function replaceTemplates(xmlDocElement) {
    let treeWalker = document.createTreeWalker(
        xmlDocElement,
        NodeFilter.SHOW_ALL,
        {
            acceptNode: function (node) {
                return !is_ignorable(node) && is_terminal(node) ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
            }
        }
    );
    let terminalNodes = [];
    let currentNode = treeWalker.nextNode();
    while (currentNode) {
        terminalNodes.push(currentNode);
        currentNode = treeWalker.nextNode();
    }
    return terminalNodes;
}

function isMandatoryForTerminalNode(node) {
    if (is_terminal(node) && !is_ignorable(node)) {
        let targetPath = getPathUntilParent($(node));
        if (targetPath) {
            if (targetPath.includes("/")) {
                let targetPathArr = targetPath.split("/").reverse();
                if (targetPathArr[1].replace(/[a-z0-9]+:/, "") === "if") {
                    return "Y";
                } else return "Y";
            } else return "N";
        }
    }
}

function loadTableData(xmlDocElement) {
    let tabledata = [];
    let id = 1;
    let terminalNodes = getTerminalElements(xmlDocElement);
    if (terminalNodes) {
        for (let terminalNode of terminalNodes) {
            let mappingRow = {};
            mappingRow.id = id++;
            mappingRow.src = getTerminalNodesValue(terminalNode).funcParam;
            mappingRow.dst = getPathUntilParent(terminalNode);
            mappingRow.isMandatory = isMandatoryForTerminalNode(terminalNode);
            mappingRow.mapType = getMappingType(terminalNode);
            tabledata.push(mappingRow);
        }
        return tabledata;
    }
}