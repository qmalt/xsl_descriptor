var fileReader = new FileReader(), xmlDocElement, mappingTable, headerTable, rowIndex, editable = false;

initMenu();

if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success! All the File APIs are supported.
} else {
    alert('The File APIs are not fully supported in this browser.');
}

fileReader.onload = function (e) {
    try {
        $('#uploadModalWindow').modal('hide');
        xmlDocElement = jQuery.parseXML(e.target.result);
        if (xmlDocElement) {
            loadHeaderTable(xmlDocElement);
            loadMappingTable(xmlDocElement);
            $("#table_toolbar").show();
            $("#fileUploadArea").hide();
        }
    } catch (e) {
        console.log(e);
    }
};


function handleFileSelect(evt) {
    let files = evt.target.files;
    fileReader.readAsText(files[0], 'utf-8');
}

function loadMappingTable(xmlDocElement) {
    let mappingTableData = loadTableData(xmlDocElement);

    let isEditable = function () {
        return editable;
    };
//create Tabulator on DOM element with id "example-table"
    mappingTable = new Tabulator("#xsl_desc_table", {
        columnVertAlign: "top", //align header contents to bottom of cell
        data: mappingTableData, //assign data to table
        layout: "fitColumns", //fit columns to width of table (optional)
        clipboard:true,
        columns: [ //Define Table Columns
            {title: "Источник", field: "src", formatter: "textarea", headerSort: false, editor: "textarea", editable: isEditable},
            {title: "Преобразование", field: "mapType", formatter: "textarea", headerSort: false, editor: "textarea", editable: isEditable},
            {title: "Назначение", field: "dst", formatter: "textarea", headerSort: false, editor: "textarea", editable: isEditable},
            {title: "Обязательность", field: "isMandatory", width: 150, align: "center", headerSort: false, editor: "input", editable: isEditable}
        ],
        rowClick: function (e, row) { //trigger an alert message when the row is clicked
            rowIndex = row.getData().id;
        }
    });
    initTableToolbar();
}

function loadHeaderTable(xmlDocElement) {
    let oracleMappingParams = getOracleMappingParams(xmlDocElement);
    let headerTableData = [];
    for (let i = 0; i < Math.max(oracleMappingParams.sources.length, oracleMappingParams.targets.length); i++) {
        let row = {};
        row.srcLocation = oracleMappingParams.sources[i] === undefined ? "" : oracleMappingParams.sources[i].location;
        row.srcRootNode = oracleMappingParams.sources[i] === undefined ? "" : oracleMappingParams.sources[i].rootNode;
        row.tgtLocation = oracleMappingParams.targets[i] === undefined ? "" : oracleMappingParams.targets[i].location;
        row.tgtRootNode = oracleMappingParams.targets[i] === undefined ? "" : oracleMappingParams.targets[i].rootNode;
        headerTableData.push(row);
    }
    headerTable = new Tabulator("#xsl_header_table", {
        columnVertAlign: "top", //align header contents to bottom of cell
        data: headerTableData, //assign data to table
        layout: "fitDataFill", //fit columns to width of table (optional)
        columns: [ //Define Table Columns
            {title: "Источник (WSDL, XSD)", field: "srcLocation", formatter: "textarea", headerSort: false},
            {title: "Корневой объект", field: "srcRootNode", formatter: "textarea", headerSort: false},
            {title: "Назначение (WSDL, XSD)", field: "tgtLocation", formatter: "textarea", headerSort: false},
            {title: "Корневой объект", field: "tgtRootNode", formatter: "textarea", headerSort: false}
        ]
    });
}

function initMenu() {
    $('#logoItem').on("click", function () {
        location.reload();
    });
    $('#openXSL').on("click", function () {
        $('#files').trigger("click");
    });
    $('#files').on("change", handleFileSelect);
}

function initTableToolbar() {
    $("#addRowBtn").on("click", function () {
        let newRowIndex = rowIndex + 1;
        mappingTable.searchRows("id", ">", rowIndex).reverse().forEach(function (element) {
            mappingTable.updateRow(element.getData().id, {id: element.getData().id + 1});
        });
        mappingTable.addRow({id: newRowIndex, src: "\n", mapType: "\n", dst: "\n", isMandatory: "\n"}, false, rowIndex);
    });
    $("#delRowBtn").on("click", function () {
        mappingTable.searchRows("id", ">", rowIndex).reverse().forEach(function (element) {
            mappingTable.updateRow(element.getData().id, {id: element.getData().id - 1});
        });
        mappingTable.deleteRow(rowIndex);
    });
    $("#editBtn").click(function () {
        editable = !editable;
    });
    $("#xlsxBtn").click(function () {
        mappingTable.download("xlsx", "data.xlsx", {sheetName: "mapping"});
    });
    $("#copyBtn").click(function () {
        mappingTable.copyToClipboard("table");
    });
}
